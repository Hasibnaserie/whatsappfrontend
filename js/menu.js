$(document).ready(function()
{
    // Create socket.
    var socket = io.connect('http://127.0.0.1:5000');
    var toggle = false;
    var messageAmount = $("#messages-amount");


    test()

    function test()
    {
        var ScrollTop = parseInt($(window).scrollTop());
        var scrollHeight = $(window).height()
        if (ScrollTop < scrollHeight)
        {
             // On chat message.
            socket.on('chatMessage', function(msg){
                var profile_picture = '<img class="p-2 profile-picture" src="data:image/png;base64, ' + msg['profilePicture'] + '">';
                // Count messages amount.
                var currentMessagesCount = parseInt(messageAmount.text());
                var messageCount = parseInt(currentMessagesCount + 1);
                messageAmount.html(messageCount);

                if (!toggle)
                {
                    {

                        $tr = "<tr class='bg-white message'><td>" + profile_picture + "</td><td class='full-message'><span class='font-weight-bold username'>" + msg['name'] + "</span><br><p class='message-content my-2 overflow-hidden'>" + msg['message'] + "</p></td><td class='text-black-50'></td><td><div class='phone-number d-inline text-white rounded-pill p-2 px-3'>" + msg['phoneNumber'] + "</td><td><i class='fas fa-cog mr-2 puretesting' id='" + msg['chatmessageId'] + "' data-toggle='modal' data-target='#myModal' style='font-size:24px; color: #609AF4'></i><a class='delete-message' id='" + msg['chatmessageId'] + "'><i class='fas fa-trash-alt' style='font-size:24px; color: #DE4D5D;'></i></a></td><td class='text-black-50'>" + msg['timestamp'] + "</td></tr>";
                        addMessage($tr);
                        // Check if the scrolltop of the message is bigger then 25, if yes enabled more toggle.
                        if ($('.message-content:first')[0].scrollHeight > 25)
                        {
                            $("<td class='text-black-50'><i class='fas fa-caret-down' style='font-size:24px;color: #609AF4;'></i> meer...</td>").appendTo("#messages tr:first td:nth-child(3)");
                        }

                    }
                }
                // Is toggled.
                else
                {
                    $tr = "<tr class='bg-white message'><td>" + profile_picture + "</td><td class='full-message'><span class='font-weight-bold username'>" + msg['name'] + "</span><br><p class='message-content overflow-hidden mb-0'>" + msg['message'] + "</p></td><td class='text-black-50'><i class='fas fa-caret-down' style='font-size:24px;color: #609AF4;'></i> meer...</td><td><div class='phone-number d-inline text-white rounded-pill p-2 px-3'>" + msg['phoneNumber'] + "</td><td><i class='fas fa-cog mr-2 puretesting' id='" + msg['chatmessageId'] + "' data-toggle='modal' data-target='#myModal' style='font-size:24px; color: #609AF4'></i><a class='delete-message' id='" + msg['chatmessageId'] + "'><i class='fas fa-trash-alt' style='font-size:24px; color: #DE4D5D;'></i></a></td><td class='text-black-50'>" + msg['timestamp'] + "</td></tr>";
                    addMessage($tr);
                    $(".profile-picture").css('width', '50px');
                    // Check if the scrolltop of the message is bigger then 25, if yes enabled more toggle.
                    if($('.message:first')[0].scrollHeight > 25)
                    {
                        $("<td class='text-black-50'><i class='fas fa-caret-down' style='font-size:24px;color: #609AF4;'></i> meer...</td>").appendTo("#messages tr:first td:nth-child(3)");
                    }
                }
            });
        }
        else
        {
            setTimeout(test, 500);
        }
    }


    // Delete message function
    $(document).on('click', '.delete-message', function()
    {
        // Fetching chatmessageId.
        var deleteMessageThis = this;
        var messageId = this.id;

        $.ajax({
            type: 'POST',
            url: '/delete_one_message',
            data: {chatMessageId: messageId},
            dataType: 'text',
            // Success.
            success: function()
            {
                // Count messages amount.
                var currentMessagesCount = parseInt(messageAmount.text())
                var messageCount = parseInt(currentMessagesCount - 1);
                $(messageAmount).html(messageCount);
                // Deleting closest table row.
                tableRow = $(deleteMessageThis).closest('tr').remove();
            }
        });
    });

    // Toggle function.
    $("#toggle").click(function(){
        // Change width to 75px.
        if (!toggle)
        {
            // Changing profile picture width.
            $(".profile-picture").css('width', '50px');
            $(".message-content").removeClass('my-2');
            $(".message-content").css('margin-bottom', '0px');
            toggle = true;
        }
        else
        {
            // Changing profile picture width.
            $(".profile-picture").css('width', '100px');
            $(".message-content").addClass('my-2');
            toggle = false;
        }
    });

    // Onclick option Icon.
    $(document).on('click', '.puretesting', function()
    {
        $("#messageId").empty();
        $("#messageId").val(this.id)
    });


    var moreToggle = false;
    $(document).on('click', '.fa-caret-down', function()
        {
            // Clicked message
            var clickedMessage = $(this).closest("tr").find(".message-content");

            if (!moreToggle)
            {
                $(clickedMessage).removeClass("overflow-hidden")
                $(clickedMessage).height("fit-content")
                moreToggle = true
            }
            else
            {
                $(clickedMessage).addClass("overflow-hidden")
                $(clickedMessage).height('25px');
                moreToggle = false
            }
        },
    );

    // Add message to messages table.
    function addMessage(message)
    {
        if ($('#messages tr').length == 0)
        {
            $('#messages').append(message);

        } else
        {
            $('#messages tr:first').before(message);
        }
    }
});
// End of on ready.

/* Delete all messages function */
$(function() {
  $('#deleteAll').bind('click', function() {
    /* Delete list */
    $("#messages").empty();
    $("#clear-screen-modal").modal('hide');
    $("#messages-amount").html(0);
    $.getJSON('/delete_all_messages',
        function(data) {
            // Code here.
    });
    return false;
  });
});

// Filter messages function.
function filterMessages()
{
  var input, filter, table, tr, td, i, txtValue;

  input = document.getElementById("search");
  filter = input.value.toUpperCase();
  table = document.getElementById("messages");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
    messages = tr[i].getElementsByTagName("td")[1];
    // phoneNumber = tr[i].getElementsByTagName("td")[3];
    if (messages) {
      txtValue = messages.textContent || messages.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      tr[i].style.display = "";
    }
    else {
      tr[i].style.display = "none";
    }
  }
  }
}